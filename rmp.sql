-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        5.6.10 - MySQL Community Server (GPL)
-- 服务器操作系统:                      Win64
-- HeidiSQL 版本:                  9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- 导出 rmp 的数据库结构
CREATE DATABASE IF NOT EXISTS `rmp` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;
USE `rmp`;

-- 导出  表 rmp.rmp_api_data_history 结构
CREATE TABLE IF NOT EXISTS `rmp_api_data_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `apiName` text COLLATE utf8mb4_unicode_ci,
  `apiIdentity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apiData` text COLLATE utf8mb4_unicode_ci,
  `saveTime` datetime DEFAULT NULL,
  `mark` text COLLATE utf8mb4_unicode_ci,
  `testTime` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `filePath` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- 正在导出表  rmp.rmp_api_data_history 的数据：~2 rows (大约)
DELETE FROM `rmp_api_data_history`;
/*!40000 ALTER TABLE `rmp_api_data_history` DISABLE KEYS */;
INSERT INTO `rmp_api_data_history` (`id`, `apiName`, `apiIdentity`, `apiData`, `saveTime`, `mark`, `testTime`, `filePath`) VALUES
	(20, '3123124', '32131231', '{"applicationResources":[{"tags":"wangyj","host":"39.108.62.206","logSize":"","freeMem":"54.26%","ioRead":"0.00MB/S","logCycle":"","cpuRequirements":"","tranQueue":"","serverType":"linux","memoryRequirements":"","networkUtilization":"","freeCpu":"88.00%","logNum":"","ioWrite":"0.00MB/S","backupResource":"","sharedMemory":"","backupNetworkUtilization":"","containerResource":""},{"tags":"wangyj","host":"39.108.62.206","logSize":"","freeMem":"54.26%","ioRead":"0.00MB/S","logCycle":"","cpuRequirements":"","tranQueue":"","serverType":"linux","memoryRequirements":"","networkUtilization":"","freeCpu":"88.00%","logNum":"","ioWrite":"0.00MB/S","backupResource":"","sharedMemory":"","backupNetworkUtilization":"","containerResource":""}],"databaseResources":[{"tags":"wangyj","host":"39.108.62.206","freeCpu":"84.14%","connect":"","freeMem":"52.29%","ioWait":"0.00%","serverType":"linux"}],"clusterResources":[{"tags":"wangyj","Inclination":"","host":"39.108.62.206","freeCpu":"82.00%","storageInc":"","freeMem":"50.80%","serverType":"linux"}]}', '2018-06-05 17:24:24', '1241241', NULL, 'exportExcel\\3123124.xlsx'),
	(21, '42141', '312412', '{"applicationResources":[{"tags":"wangyj","host":"39.108.62.206","logSize":"","freeMem":"52.29%","ioRead":"0.00MB/S","logCycle":"","cpuRequirements":"","tranQueue":"","serverType":"linux","memoryRequirements":"","networkUtilization":"","freeCpu":"84.14%","logNum":"","ioWrite":"0.00MB/S","backupResource":"","sharedMemory":"","backupNetworkUtilization":"","containerResource":""}],"databaseResources":[{"tags":"wangyj","host":"39.108.62.206","freeCpu":"88.00%","connect":"","freeMem":"54.26%","ioWait":"0.00%","serverType":"linux"}],"clusterResources":[{"tags":"wangyj","Inclination":"","host":"39.108.62.206","freeCpu":"89.91%","storageInc":"","freeMem":"55.53%","serverType":"linux"}]}', '2018-06-05 17:36:42', '1412', NULL, 'exportExcel\\42141.xlsx');
/*!40000 ALTER TABLE `rmp_api_data_history` ENABLE KEYS */;

-- 导出  表 rmp.rmp_file_info 结构
CREATE TABLE IF NOT EXISTS `rmp_file_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fileName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `filePath` text COLLATE utf8mb4_unicode_ci,
  `configId` int(11) DEFAULT NULL,
  `fileSize` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `serverCount` int(5) DEFAULT NULL,
  `infoCount` int(11) DEFAULT NULL,
  `serverInfos` text COLLATE utf8mb4_unicode_ci,
  `mark` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `rmp_file_info_fk_config_id` (`configId`),
  CONSTRAINT `rmp_file_info_fk_config_id` FOREIGN KEY (`configId`) REFERENCES `rmp_user_config` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- 正在导出表  rmp.rmp_file_info 的数据：~2 rows (大约)
DELETE FROM `rmp_file_info`;
/*!40000 ALTER TABLE `rmp_file_info` DISABLE KEYS */;
INSERT INTO `rmp_file_info` (`id`, `fileName`, `filePath`, `configId`, `fileSize`, `createTime`, `serverCount`, `infoCount`, `serverInfos`, `mark`) VALUES
	(8, '1525855442740_xuwang1314.json', 'files/1525855442740_xuwang1314.json', 18, '6.05KB', '2018-05-09 16:44:03', 4, 29, '[{"port":22,"tags":"wangyj","host":"39.108.62.206","count":10,"memInfo":2054216,"uname":"Linux","lastTime":"2018-05-09 16:43:56","password":"111111","info":{"time":"2018-05-09 16:44:02","freeCpu":"98","tcpInfo":{"CLOSE_WAIT":"1","LISTEN":"12","TIME_WAIT":"0","ESTABLISHED":"6"},"freeMem":"60.9","ioWait":"0","networkInfo":{"tx":"0","rx":"0"},"diskInfo":{"rootDisk":"24","userDisk":"0"},"ioInfo":{"ioWrite":"0.01","ioRead":"0.00"}},"serverType":"linux","startTime":"2018-05-09 16:43:03","id":5,"username":"www","connectStatus":"true","cpuInfo":1,"mountDevices":["vda"],"parameters":"{\\"javaHome\\":\\"/usr/java/default\\"}","viewId":0},{"port":22,"tags":"wangyj","host":"39.108.62.206","count":8,"memInfo":2054216,"uname":"Linux","lastTime":"2018-05-09 16:43:56","password":"111111","info":{"time":"2018-05-09 16:44:02","freeCpu":"99","tcpInfo":{"CLOSE_WAIT":"1","LISTEN":"12","TIME_WAIT":"0","ESTABLISHED":"6"},"freeMem":"61","ioWait":"0","networkInfo":{"tx":"0","rx":"0"},"diskInfo":{"rootDisk":"24","userDisk":"0"},"ioInfo":{"ioWrite":"0.01","ioRead":"0.00"}},"serverType":"linux","startTime":"2018-05-09 16:43:13","id":5,"username":"www","connectStatus":"true","cpuInfo":1,"mountDevices":["vda"],"parameters":"{\\"javaHome\\":\\"/usr/java/default\\"}","viewId":1},{"port":22,"tags":"wangyj","host":"39.108.62.206","count":6,"memInfo":2054216,"uname":"Linux","lastTime":"2018-05-09 16:43:56","password":"111111","info":{"time":"2018-05-09 16:44:01","freeCpu":"99","tcpInfo":{"CLOSE_WAIT":"1","LISTEN":"12","TIME_WAIT":"0","ESTABLISHED":"6"},"freeMem":"61","ioWait":"0","networkInfo":{"tx":"0","rx":"0"},"diskInfo":{"rootDisk":"24","userDisk":"0"},"ioInfo":{"ioWrite":"0.01","ioRead":"0.00"}},"serverType":"linux","startTime":"2018-05-09 16:43:22","id":5,"username":"www","connectStatus":"true","cpuInfo":1,"mountDevices":["vda"],"parameters":"{\\"javaHome\\":\\"/usr/java/default\\"}","viewId":2},{"port":22,"tags":"wangyj","host":"39.108.62.206","count":5,"memInfo":2054216,"uname":"Linux","lastTime":"2018-05-09 16:43:56","password":"111111","info":{"time":"2018-05-09 16:44:02","freeCpu":"98","tcpInfo":{"CLOSE_WAIT":"1","LISTEN":"12","TIME_WAIT":"0","ESTABLISHED":"6"},"freeMem":"60.9","ioWait":"0","networkInfo":{"tx":"0","rx":"0"},"diskInfo":{"rootDisk":"24","userDisk":"0"},"ioInfo":{"ioWrite":"0.01","ioRead":"0.00"}},"serverType":"linux","startTime":"2018-05-09 16:43:30","id":5,"username":"www","connectStatus":"true","cpuInfo":1,"mountDevices":["vda"],"parameters":"{\\"javaHome\\":\\"/usr/java/default\\"}","viewId":3}]', '这是备注');
/*!40000 ALTER TABLE `rmp_file_info` ENABLE KEYS */;

-- 导出  表 rmp.rmp_real_time_info 结构
CREATE TABLE IF NOT EXISTS `rmp_real_time_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `serverId` int(11) DEFAULT NULL,
  `configId` int(11) DEFAULT NULL,
  `infoJson` text COLLATE utf8mb4_unicode_ci,
  `saveTime` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `rmp_real_time_info_fk_server_id` (`serverId`),
  KEY `rmp_real_time_info_fk_config_id` (`configId`),
  CONSTRAINT `rmp_real_time_info_fk_config_id` FOREIGN KEY (`configId`) REFERENCES `rmp_user_config` (`id`) ON DELETE CASCADE,
  CONSTRAINT `rmp_real_time_info_fk_server_id` FOREIGN KEY (`serverId`) REFERENCES `rmp_server_info` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- 正在导出表  rmp.rmp_real_time_info 的数据：~0 rows (大约)
DELETE FROM `rmp_real_time_info`;
/*!40000 ALTER TABLE `rmp_real_time_info` DISABLE KEYS */;
INSERT INTO `rmp_real_time_info` (`id`, `serverId`, `configId`, `infoJson`, `saveTime`) VALUES
	(1, 6, 17, '账户余额', '2018-03-29 17:10:19');
/*!40000 ALTER TABLE `rmp_real_time_info` ENABLE KEYS */;

-- 导出  表 rmp.rmp_server_info 结构
CREATE TABLE IF NOT EXISTS `rmp_server_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `host` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `realHost` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `port` int(6) DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `serverType` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci,
  `createTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `mark` text COLLATE utf8mb4_unicode_ci,
  `lastUseTime` datetime DEFAULT NULL,
  `tags` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- 正在导出表  rmp.rmp_server_info 的数据：~1 rows (大约)
DELETE FROM `rmp_server_info`;
/*!40000 ALTER TABLE `rmp_server_info` DISABLE KEYS */;
INSERT INTO `rmp_server_info` (`id`, `host`, `realHost`, `port`, `username`, `password`, `serverType`, `parameters`, `createTime`, `mark`, `lastUseTime`, `tags`) VALUES
	(5, '39.108.62.206', NULL, 22, 'www', '111111', 'linux', '{"javaHome":"/usr/java/default"}', '2018-02-05 16:45:20', NULL, '2018-05-09 16:43:30', 'wangyj'),
	(6, '127.0.0.1', NULL, 7001, 'weblogic', 'weblogic!123', 'weblogic', '{"javaHome":"/opt/jdk1.7/bin","linuxLoginUsername":"","linuxLoginPassword":"","startScriptPath":""}', '2018-03-28 14:58:35', NULL, '2018-03-28 15:58:08', 'wangyj'),
	(7, '127.0.0.1', NULL, 8999, 'monitorRole', 'tomcat', 'tomcat', '{"webPort":"8080","projectName":"/atp","javaHome":"/opt/jdk1.7","linuxLoginUsername":"","linuxLoginPassword":"","startScriptPath":""}', '2018-03-30 00:54:22', NULL, '2018-03-30 11:06:41', 'wangyj'),
	(8, '192.168.42.249', NULL, 20880, NULL, NULL, 'dubbo', '{"javaHome":"/opt/jdk1.7","linuxLoginUsername":"","linuxLoginPassword":"","startScriptPath":""}', '2018-03-30 19:51:14', NULL, '2018-04-25 15:38:23', 'wangyj'),
	(9, '10.243.23.12', NULL, 10150, NULL, NULL, 'dubbo', NULL, '2018-04-17 09:51:24', NULL, NULL, 'wangyj'),
	(10, '152.55.249.128', NULL, 22, 'root', 'test!2013', 'linux', NULL, '2018-04-19 11:07:24', NULL, '2018-05-07 14:54:34', 'wangyj'),
	(11, '119.23.207.50', NULL, 22, 'root', '5t6y#89kL', 'linux', NULL, '2018-04-25 14:41:49', NULL, '2018-04-25 14:45:09', 'duanjy'),
	(12, '122.112.246.117', NULL, 6379, NULL, NULL, 'redis', '{"mode":"l","linuxLoginUsername":"","linuxLoginPassword":"","linuxLoginPort":"22","redisCliPath":""}', '2018-06-04 10:30:50', NULL, '2018-06-04 10:44:59', '华为云');
/*!40000 ALTER TABLE `rmp_server_info` ENABLE KEYS */;

-- 导出  表 rmp.rmp_user_config 结构
CREATE TABLE IF NOT EXISTS `rmp_user_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userKey` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `configInfoJson` text COLLATE utf8mb4_unicode_ci,
  `createTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- 正在导出表  rmp.rmp_user_config 的数据：~4 rows (大约)
DELETE FROM `rmp_user_config`;
/*!40000 ALTER TABLE `rmp_user_config` DISABLE KEYS */;
INSERT INTO `rmp_user_config` (`id`, `userKey`, `configInfoJson`, `createTime`) VALUES
	(17, 'sdw787235', '', '2018-03-20 14:28:15'),
	(18, 'xuwang1314', '{"alert":{"dubbo":{"threadActiveCount":500,"memoryUsedPercent":100},"redis":{"totalPercentSecondCurrent":5000000,"keyspaceHitRateCurrent":50,"usedCpu":3600},"tomcat":{"threadCurrentBusyCount":10},"jvm":{"oldSpacePercent":100,"blockedThreadCount":10,"edenSpacePercent":100,"permSpacePercent":100},"linux":{"freeCpu":10,"CLOSE_WAIT":20000,"freeMem":10,"ioWait":20,"userDisk":90,"TIME_WAIT":20000},"weblogic":{"pendingCount":1,"freePercent":10,"waitingForConnectionCurrentCount":1}},"export":{},"monitoring":{"refreshIntervalTime":6000,"autoClearDataFlag":false,"maxInfoDataCount":20000,"autoSaveDataFlag":true,"alertMonitorIntervalTime":10000,"cacheDataDetectionIntervalTime":30000,"playNoticIntervalTime":4000},"other":{"home":"http://www.baidu.com","alertInfoTipShow":"true","apiUrl":"http://127.0.0.1/report/analyzeApiData"}}', '2018-03-28 10:43:06'),
	(20, 'xuwang199', '', '2018-04-19 11:13:14');
/*!40000 ALTER TABLE `rmp_user_config` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
