package com.dcits.business.server.redis;

import com.dcits.business.server.MonitoringInfo;

public class RedisMonitoringInfo extends MonitoringInfo {
	/**
	 * 版本号<br>
	 * key: redis_version
	 */
	private String version;
	/**
	 * 进程号<br>
	 * key: process_id
	 */
	private String processId;
	
	/**
	 * 运行时间，天
	 * <br>key: uptime_in_days
	 */
	private String upTime;
	
	/**
	 * 当前连接的客户端数量<br>
	 * key: connected_clients
	 */
	private String clientCount;
	
	/**
	 * 分配使用的内存量，单位B，需要转换成kb
	 * key: used_memory
	 */
	private String usedMemory;
	
	/**
	 * 历史分配使用的最大内存量，单文B，需要转换成kb
	 * key: used_memory_peak
	 */
	private String usedMemoryMax;
	
	/**
	 * 使用CPU百分百-系统
	 * <br>key: used_cpu_sys
	 */
	private String usedCpuSys;
	
	/**
	 * 使用CPU百分百-用户
	 * <br>key: used_cpu_user
	 */
	private String usedCpuUser;
	
	/**
	 * 总使用CPU百分比<br>
	 * usedCpuSys + usedCpuUser
	 */
	private String usedCpu;
	
	/**
	 * 历史执行命令数量<br>
	 * key: total_commands_processed
	 */
	private String totalCommandCount;
	
	/**
	 * 当前时段的QPS<br>
	 *  (totalCommandCount2 - totalCommandCount1) / (timestamp2 - timestamp1) / 1000
	 */
	private String totalPercentSecondCurrent = "0";
	
	/**
	 * 命中key的总数量<br>
	 * key： keyspace_hits
	 * 
	 */
	private String keyspaceHits;
	/**
	 * 未命中key的总数量<br>
	 * key: keyspace_misses
	 */
	private String keyspaceMisses;
	
	/**
	 * 当前时段中命中成功百分比<br>
	 * (keyspaceHits2 - keyspaceHits1) / ((keyspaceHits2 - keyspaceHits1) + (keyspaceMisses2 - keyspaceMisses1)) * 100
	 */
	private String keyspaceHitRateCurrent = "0";
	
	/**
	 * 运行以来过期的 key 的数量<br>
	 * key: expired_keys
	 */
	private String expiredKeys;
	
	/**
	 * 运行以来删除过的key的数量<br>
	 * key: evicted_keys
	 */
	private String evictedKeys;
	
	/**
	 * 各个数据库当前保存的key总数量<br>
	 * db0:keys=7,expires=0,avg_ttl=0
	 */
	private String dbKeyCount;
	
	/**
	 * 当前实例的角色，master或者slave
	 * <br>key: role
	 */
	private String role;
	
	/**
	 * 本次保存时间戳，用于计算QPS
	 */
	private long timestamp = 0L;

	public String getTotalPercentSecondCurrent() {
		return totalPercentSecondCurrent;
	}

	public void setTotalPercentSecondCurrent(String totalPercentSecondCurrent) {
		this.totalPercentSecondCurrent = totalPercentSecondCurrent;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getProcessId() {
		return processId;
	}

	public void setProcessId(String processId) {
		this.processId = processId;
	}

	public String getUpTime() {
		return upTime;
	}

	public void setUpTime(String upTime) {
		this.upTime = upTime;
	}

	public String getClientCount() {
		return clientCount;
	}

	public void setClientCount(String clientCount) {
		this.clientCount = clientCount;
	}

	public String getUsedMemory() {
		return usedMemory;
	}

	public void setUsedMemory(String usedMemory) {
		this.usedMemory = usedMemory;
	}

	public String getUsedMemoryMax() {
		return usedMemoryMax;
	}

	public void setUsedMemoryMax(String usedMemoryMax) {
		this.usedMemoryMax = usedMemoryMax;
	}

	public String getUsedCpuSys() {
		return usedCpuSys;
	}

	public void setUsedCpuSys(String usedCpuSys) {
		this.usedCpuSys = usedCpuSys;
	}

	public String getUsedCpuUser() {
		return usedCpuUser;
	}

	public void setUsedCpuUser(String usedCpuUser) {
		this.usedCpuUser = usedCpuUser;
	}

	public String getUsedCpu() {
		return usedCpu;
	}

	public void setUsedCpu(String usedCpu) {
		this.usedCpu = usedCpu;
	}

	public String getTotalCommandCount() {
		return totalCommandCount;
	}

	public void setTotalCommandCount(String totalCommandCount) {
		this.totalCommandCount = totalCommandCount;
	}

	public String getKeyspaceHits() {
		return keyspaceHits;
	}

	public void setKeyspaceHits(String keyspaceHits) {
		this.keyspaceHits = keyspaceHits;
	}

	public String getKeyspaceMisses() {
		return keyspaceMisses;
	}

	public void setKeyspaceMisses(String keyspaceMisses) {
		this.keyspaceMisses = keyspaceMisses;
	}

	public String getKeyspaceHitRateCurrent() {
		return keyspaceHitRateCurrent;
	}

	public void setKeyspaceHitRateCurrent(String keyspaceHitRateCurrent) {
		this.keyspaceHitRateCurrent = keyspaceHitRateCurrent;
	}

	public String getExpiredKeys() {
		return expiredKeys;
	}

	public void setExpiredKeys(String expiredKeys) {
		this.expiredKeys = expiredKeys;
	}

	public String getEvictedKeys() {
		return evictedKeys;
	}

	public void setEvictedKeys(String evictedKeys) {
		this.evictedKeys = evictedKeys;
	}

	public String getDbKeyCount() {
		return dbKeyCount;
	}

	public void setDbKeyCount(String dbKeyCount) {
		this.dbKeyCount = dbKeyCount;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return "RedisMonitoringInfo [version=" + version + ", processId=" + processId + ", upTime=" + upTime
				+ ", clientCount=" + clientCount + ", usedMemory=" + usedMemory + ", usedMemoryMax=" + usedMemoryMax
				+ ", usedCpuSys=" + usedCpuSys + ", usedCpuUser=" + usedCpuUser + ", usedCpu=" + usedCpu
				+ ", totalCommandCount=" + totalCommandCount + ", totalPercentSecondCurrent="
				+ totalPercentSecondCurrent + ", keyspaceHits=" + keyspaceHits + ", keyspaceMisses=" + keyspaceMisses
				+ ", keyspaceHitRateCurrent=" + keyspaceHitRateCurrent + ", expiredKeys=" + expiredKeys
				+ ", evictedKeys=" + evictedKeys + ", dbKeyCount=" + dbKeyCount + ", role=" + role + ", timestamp="
				+ timestamp + "]";
	}

}
