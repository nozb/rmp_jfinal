package com.dcits.constant;

public class ConstantReturnCode {
	
	public static final Integer SUCCESS = 0;
	
	public static final Integer SYSTEM_ERROR = 1;
	
	public static final Integer VALIDATE_FAIL = 2;
	
	public static final Integer DB_OP_ERROR = 3;
	
	public static final Integer SERVER_CONNECT_FAILED = 4;
}
